import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { ethers, upgrades } from 'hardhat';
import {
  StrategyCakeTokenTokenFactory,
} from '../typechain';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
    /*
  ░██╗░░░░░░░██╗░█████╗░██████╗░███╗░░██╗██╗███╗░░██╗░██████╗░
  ░██║░░██╗░░██║██╔══██╗██╔══██╗████╗░██║██║████╗░██║██╔════╝░
  ░╚██╗████╗██╔╝███████║██████╔╝██╔██╗██║██║██╔██╗██║██║░░██╗░
  ░░████╔═████║░██╔══██║██╔══██╗██║╚████║██║██║╚████║██║░░╚██╗
  ░░╚██╔╝░╚██╔╝░██║░░██║██║░░██║██║░╚███║██║██║░╚███║╚██████╔╝
  ░░░╚═╝░░░╚═╝░░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░╚══╝╚═╝╚═╝░░╚══╝░╚═════╝░
  Check all variables below before execute the deployment script
  */

  // ----------------!!!---------------------
  // SOME ADDRESSES ARE PLACED INSIDE SOL FILE
  // ----------------!!!---------------------
  const CONTROLLER_ADDR = '0x6400d53adbF6f043aCdD6EE324610239A151708D';
  const GOVERNANCE_ADDR = '0xd1175037A995F3A371894A57b415215845ec306F'; // our deployer address
  const STRATEGIST_ADDR = '0xd1175037A995F3A371894A57b415215845ec306F'; // our deployer address (strategist ourselfs)
  const TOKEN_WANT_ADDR = '0x472620F8D26d20A50B2f35180A3f9bc6cEcBB7E9'; // token address
  const FARM_LP_ADDRESS = '0x0cfc7D6FFF1B3a9EA126203c9a6Ee8956A404099'; // farm address (token)
  const A_TOKEN_ADDRESS = '0x2b8ff854c5e16cf35b9a792390cc3a2a60ec9ba2'; // other toekn to change to (address). For example our token which is want address for now
  const B_TOKEN_ADDRESS = '0x472620F8D26d20A50B2f35180A3f9bc6cEcBB7E9'; // other toekn to change to (address). For example our token which is want address for now
  const CAKE_TO_TOKEN_STRATEGY_ADDRESS = '0x944e4ad3908248CEC6307d3Bc43e67ca292453b5'; // other toekn to change to (address). For example our token which is want address for now










  console.log(">> Deploying an upgradable StrategyCakeTokenToken contract");
  const StrategyCakeTokenToken = (await ethers.getContractFactory(
    "StrategyCakeTokenToken",
    (await ethers.getSigners())[0],
  )) as StrategyCakeTokenTokenFactory;
  const strategyCakeTokenToken = await upgrades.deployProxy(StrategyCakeTokenToken, 
    [GOVERNANCE_ADDR, STRATEGIST_ADDR, CONTROLLER_ADDR, TOKEN_WANT_ADDR, 
      A_TOKEN_ADDRESS, B_TOKEN_ADDRESS, FARM_LP_ADDRESS, CAKE_TO_TOKEN_STRATEGY_ADDRESS]);
  await strategyCakeTokenToken.deployed();
  console.log(`✔ Deployed at ${strategyCakeTokenToken.address}`);
};

export default func;
func.tags = ['StrategyCakeTokenToken'];