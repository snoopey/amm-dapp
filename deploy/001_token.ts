import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { ethers, upgrades } from 'hardhat';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;

  const { deployer } = await getNamedAccounts();
  const HRTOKEN_START_RELEASE = '1620656026';
  const HRTOKEN_END_RELEASE = '1652192026';

  await deploy('Letty', {
    from: deployer,
    args: [
      HRTOKEN_START_RELEASE, HRTOKEN_END_RELEASE
    ],
    log: true,
    deterministicDeployment: false,
  });

  const letty = await deployments.get('Letty');

  console.log(`✔ Letty done:`);
  console.log(`✔ Deployed at ${letty.address}`);

};

export default func;
func.tags = ['Token'];