import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { ethers, upgrades } from 'hardhat';
import {
  StrategyTokenFactory,
} from '../typechain';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
    /*
  ░██╗░░░░░░░██╗░█████╗░██████╗░███╗░░██╗██╗███╗░░██╗░██████╗░
  ░██║░░██╗░░██║██╔══██╗██╔══██╗████╗░██║██║████╗░██║██╔════╝░
  ░╚██╗████╗██╔╝███████║██████╔╝██╔██╗██║██║██╔██╗██║██║░░██╗░
  ░░████╔═████║░██╔══██║██╔══██╗██║╚████║██║██║╚████║██║░░╚██╗
  ░░╚██╔╝░╚██╔╝░██║░░██║██║░░██║██║░╚███║██║██║░╚███║╚██████╔╝
  ░░░╚═╝░░░╚═╝░░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░╚══╝╚═╝╚═╝░░╚══╝░╚═════╝░
  Check all variables below before execute the deployment script
  */

  // ----------------!!!---------------------
  // SOME ADDRESSES ARE PLACED INSIDE SOL FILE
  // ----------------!!!---------------------
  const CONTROLLER_ADDR = '0x6400d53adbF6f043aCdD6EE324610239A151708D';









  console.log(">> Deploying an upgradable StrategyToken contract");
  const StrategyToken = (await ethers.getContractFactory(
    "StrategyToken",
    (await ethers.getSigners())[0],
  )) as StrategyTokenFactory;
  const strategyToken = await upgrades.deployProxy(StrategyToken, [CONTROLLER_ADDR]);
  await strategyToken.deployed();
  console.log(`✔ Deployed at ${strategyToken.address}`);
};

export default func;
func.tags = ['StrategyToken'];